var numberArr = [];
function themSo(){
    var number = document.getElementById("txt-number").value*1;
    numberArr.push(number);
    document.getElementById("themSo").innerHTML = numberArr;
}
function tinhTongSoDuong(){
    var total = 0;
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]>0){
            total += numberArr[index];
        }
    }
  document.getElementById("tongSoDuong").innerHTML=total;
}
function demSoDuong(){
    var slSoDuong = 0;
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]>0){
            slSoDuong += 1;
        }
    }
  document.getElementById("demSoDuong").innerHTML=slSoDuong;
}
function timSoNhoNhat(){
    var soNhoNhat = numberArr[0];
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]<=soNhoNhat){
            soNhoNhat = numberArr[index];
        }
    }
    document.getElementById("soNhoNhat").innerHTML = soNhoNhat;
}
function timDuongSoNhoNhat(){
    var soDuongNhoNhat = numberArr[0];
    for(var index=0;index<numberArr.length;index++){
      if(numberArr[index]>0){
        if(numberArr[index]<=soDuongNhoNhat){
            soDuongNhoNhat = numberArr[index];
        }
      }
        
    }
    document.getElementById("soDuongNhoNhat").innerHTML = soDuongNhoNhat;
}
function timSoChanCuoiCung(){
    var soChanArr = [];
    var soChanCuoiCung = 0;
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]%2==0){
            soChanArr.push(numberArr[index]);
        }
    }
    if(soChanArr.length==0){
        soChanCuoiCung = -1;
    }else{

        soChanCuoiCung = soChanArr[soChanArr.length-1];
    }
    document.getElementById("soChanCuoiCung").innerHTML = soChanCuoiCung;
}
function doiCho(){
    var num1 = document.getElementById("num1").value*1;
    var num2 = document.getElementById("num2").value*1;
    var temp=0;
    temp = numberArr[num2];
    numberArr[num2] = numberArr[num1];
    numberArr[num1] = temp;
    document.getElementById("doiCho").innerHTML = numberArr;
}
function sapXep(){
    // numberArr.sort((a,b)=>a-b)
    // document.getElementById("sapXep").innerHTML = numberArr;
    var temp=0;
    for(var i=0;i<numberArr.length;i++){
        for(var j=0;j<numberArr.length-i;j++){
           if(numberArr[j]>numberArr[j+1]) {
            temp=numberArr[j];
            numberArr[j]=numberArr[j+1];
            numberArr[j+1]=temp;
           }
        }
    }
    document.getElementById("sapXep").innerHTML = numberArr;
}
function timSoNguyenToDauTien(){
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]==2){
            document.getElementById("soNguyenTo").innerHTML = numberArr[index];
            break;
        }else if(numberArr[index]>1&&numberArr[index]%2!==0){
            document.getElementById("soNguyenTo").innerHTML = numberArr[index];
            break;
        }else{
            document.getElementById("soNguyenTo").innerHTML = "-1";
        }
    }  
}
var newArr =[];
function themSoNewArr(){
    var newNumber = document.getElementById("newNumber").value*1;
    newArr.push(newNumber);
    document.getElementById("newArr").innerHTML = newArr;
}
function demSLSoNguyen(){
    var soLuongSoNguyen = 0;
    for(var index=0;index<newArr.length;index++){
        if(Number.isInteger(newArr[index])==true){
            soLuongSoNguyen++;
        }
    }
    document.getElementById("soLuongSoNguyen").innerHTML = soLuongSoNguyen;
}
function soSanhAmDuong(){
    var soAm = 0;
    var soDuong =0;
    for(var index=0;index<numberArr.length;index++){
        if(numberArr[index]>0){
            soDuong++;
        }else{
            soAm++;
        }
    }
    if(soDuong==soAm){
        document.getElementById("soSanhAmDuong").innerHTML = "số dương = số âm";
    }else if(soDuong<soAm){
        document.getElementById("soSanhAmDuong").innerHTML = "số dương < số âm";
    }else{
        document.getElementById("soSanhAmDuong").innerHTML = "số dương > số âm";
    }
}